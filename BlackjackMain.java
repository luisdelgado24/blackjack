/**
 * @author Luis Delgado   
 * This class is the driver for the whole game. Once inside BlackjackManager the
 * user will decide whether to keep playing new rounds or not.
 */

public class BlackjackMain 
{
	public static void main(String[] args) 
	{
		BlackjackManager game = new BlackjackManager();
		game.start();
	}
}
