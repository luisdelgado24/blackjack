import java.util.*;

/**
 * @author luisdelgado
 *
 * @program game of blackjack
 * This class carries out the game itself
 */

public class BlackjackManager 
{
	private static double myPiggyBank; 
	private Scanner sc;
	private double myCurrentBet;
	private Hand playerHand;
	private Hand dealerHand;
	private Deck d;

	/**
	 *  Kicks off the game by dealing the cards and calling decisionMaker to start playing. After a round 
	 *  is finished it asks the user whether a new round should be started or not.
	 *  <br>
	 *  pre: none</br>
	 *  post: A round has been played
	 */
	public void start() 
	{
		String anotherRound = "";
		d = new Deck();

		sc = new Scanner(System.in);
		myPiggyBank = 0;

		do 
		{
			System.out.print("\nWelcome.\n\nHow much will you be betting? Enter amount: $");
			myCurrentBet = sc.nextDouble();
			playerHand = new Hand();
			dealerHand = new Hand();

			playerHand.addCard(d.dealCard());
			dealerHand.addCard(d.dealCard());
			playerHand.addCard(d.dealCard());
			dealerHand.addCard(d.dealCard());

			//Get the sum of the player's first two cards
			int sumOfPlayerCards = getOptimalSum(playerHand);

			//Output
			System.out.println("\nMy hand:\n"+dealerHand.myHand.get(0)+"\n-Covered card-\n("+dealerHand.myHand.get(0).worth+")");
			System.out.println("\nYour hand:\n"+playerHand.myHand.get(0)+"\n"+playerHand.myHand.get(1)+"\n("+sumOfPlayerCards+")");

			//Execute the dealer's complete hand for the current round 
			int sumOfDealerCards = executeDealerHand();

			//Ask for player's decision
			playRound(sumOfDealerCards, sumOfPlayerCards);

			System.out.print("\nDo you want to play again? Enter 'y' for \"yes\", anything else to reject:  ");
			anotherRound = sc.next();
		} while(anotherRound.equals("y"));
	}

	/**
	 *  This method asks the user if he/she wants to split the hand (if possible) and, if so, calls 
	 *  splitHand in order to play the split hands. Otherwise, if splitting does not take place, a normal round is 
	 *  played by calling playNormalHand 
	 *  <br>
	 *  pre: none</br>
	 *  post: Player makes decision for current round
	 *  @param sumOfDealerCards current sum of dealer's cards; sumOfPlayerCards current sum of player's cards
	 */
	private void playRound(int sumOfDealerCards, int sumOfPlayerCards) 
	{
		if(playerHand.myHand.get(0).worth == playerHand.myHand.get(1).worth) 
		{
			System.out.print("\nSplit? Enter \"y\" to affirm, \"n\" to reject: ");
			String answer = sc.next();

			while(!checkInput(answer, true, false)) 
			{
				System.out.print("\nError in input. Enter \"y\" to affirm or \"n\" to reject: ");
				answer = sc.next();
			}

			if(answer.equals("y")) 
			{
				splitHand();
				return;
			}
		}

		//Splitting did not take place
		String answer = "s";
		if(sumOfPlayerCards != 21) 
		{
			System.out.print("\nEnter \"h\" (for 'hit me'), \"d\" (for 'double'), or \"s\" (for 'stay'): ");
			answer = sc.next();	 
			while(!checkInput(answer, false, true)) 
			{
				System.out.print("\nError in input. Enter \"h\" (for 'hit me'), \"d\" (for 'double'), or \"s\" (for 'stay'): ");
				answer = sc.next();	 
			}
		}
		else
		{
			//Blackjack
			myCurrentBet *= 1.5;
		}
		
		playHand(sumOfPlayerCards, sumOfDealerCards, answer);

	}

	/**
	 *  First step in order to play the hands into which the player's original hand will be divided into
	 *  <br>
	 *  pre: none</br>
	 *  post: hand is split into two sub-hands
	 *  @param dealerSumOfCards dealer's current sum of cards
	 */
	private void splitHand() 
	{
		playerHand.addCard(d.dealCard());
		playerHand.addCard(d.dealCard());
		displaySplitHand(playerHand);
		int sumOfFirstHand = cardsAdder(playerHand.myHand.get(0).worth, playerHand.myHand.get(2).worth);
		int sumOfSecondHand = cardsAdder(playerHand.myHand.get(1).worth, playerHand.myHand.get(3).worth);
		double multiplierFirstHand = 1.0;  
		double multiplierSecondHand = 1.0;  

		if(sumOfFirstHand != 21) 
		{
			//Play first hand
			System.out.print("\nFor the "+ sumOfFirstHand + " enter \"h\" (for 'hit me'), \"d\" (for 'double'), or \"s\" (for 'stay'): ");
			String choice = sc.next();
			while(!checkInput(choice, false, true)) 
			{
				System.out.print("\nError in input. For the "+ sumOfFirstHand + " enter \"h\" (for 'hit me'), \"d\" (for 'double'), or \"s\" (for 'stay'): ");
				choice = sc.next();
			}
			if(!choice.equals("s"))
			{
				//deal card
				sumOfFirstHand = hitAfterSplit(handSplitter(true), sumOfFirstHand, choice);
			}
			if(choice.equals("d"))
			{
				//doubled
				multiplierFirstHand += 1.0;
			}
		}
		else
		{
			//Blackjack
			multiplierFirstHand += .5;
		}

		if(sumOfSecondHand != 21) 
		{
			//Play second hand
			System.out.print("\nFor the "+ sumOfSecondHand + " enter \"h\" (for 'hit me'), \"d\" (for 'double'), or \"s\" (for 'stay'): ");
			String choice = sc.next();
			while(!checkInput(choice, false, true)) 
			{
				System.out.print("\nError in input. For the "+ sumOfSecondHand + " enter \"h\" (for 'hit me'), \"d\" (for 'double'), or \"s\" (for 'stay'): ");
				choice = sc.next();
			}
			if(!choice.equals("s"))
			{
				//deal card
				sumOfSecondHand = hitAfterSplit(handSplitter(false), sumOfFirstHand, choice);
			}
			if(choice.equals("d"))
			{
				//doubled
				multiplierSecondHand += 1.0;
			}
		}
		else
		{
			//Blackjack
			multiplierSecondHand += .5;
		}

		int sumOfDealerCards = displayDealerHand();

		double[] parameters = {sumOfFirstHand, 
				sumOfSecondHand, 
				sumOfDealerCards, 
				multiplierFirstHand, 
				multiplierSecondHand};

		determineWinnerAfterSplit(parameters);
	}

	/**
	 *  This method determines who wins in a round where the user split.
	 *  <br>
	 *  pre: none </br>
	 *  post: The user's piggy bank is adjusted according to the results
	 *  @param firstHand sum of player's first hand; secondHand sum of player's second hand; dealerHand sum of dealer's cards
	 */
	private void determineWinnerAfterSplit (double[] parameters) 
	{
		int firstHand = (int) parameters[0];
		int secondHand = (int) parameters[1];
		int sumOfDealerCards = (int) parameters[2];
		double multiplierFirstHand = parameters[3];
		double multiplierSecondHand = parameters[4];

		if((firstHand <= 21 && secondHand <= 21 && sumOfDealerCards <= 21 &&
				((sumOfDealerCards > firstHand && sumOfDealerCards < secondHand) 
				|| (sumOfDealerCards < firstHand && sumOfDealerCards > secondHand)))
				|| ((firstHand <= 21 && sumOfDealerCards < firstHand && secondHand > 21) 
				|| (secondHand <= 21 && sumOfDealerCards < secondHand && firstHand > 21))
				|| (sumOfDealerCards > 21 && ((firstHand <= 21 && secondHand > 21) || (secondHand <= 21 && firstHand > 21))))
		{
			//Condition reads: "If (both player hands are less than or equal to 21 AND dealer's hand is less than
			//or equal to 21 AND dealer's hand is less than one of the player hands and more than the other) OR (one 
			//of the player hands is over 21 but the other one beats the dealer's hand) OR 
			//(dealer's hand is over 21 but so is one of the player's hand)-- tie
			System.out.println("\nPush.");
		}
		else if((sumOfDealerCards > 21 && firstHand <= 21 && secondHand <= 21) || (firstHand <= 21 && sumOfDealerCards < firstHand 
				&& secondHand <= 21 && sumOfDealerCards < secondHand))
		{
			//Condition reads: "If dealer is over 21 and both player's hands aren't OR dealer's hand is under 21
			//but it is less than both player's hand
			myCurrentBet *= (multiplierFirstHand + multiplierSecondHand);
			myPiggyBank += myCurrentBet;
			System.out.println("\nYou beat me this round, nice going. Your balance is $" + myPiggyBank + ".");
		}
		else if(sumOfDealerCards <= 21 && sumOfDealerCards == firstHand && secondHand <= 21 && secondHand > firstHand)
		{
			//Condition reads: If dealer hand is less than or equal to 21 AND dealer hand is equal to
			//first hand AND first hand is less than second hand AND second hand is less than or equal to 21
			//--Player wins with second hand
			myCurrentBet *= multiplierSecondHand;
			myPiggyBank += myCurrentBet;
			System.out.println("\nYou beat me this round, nice going. Your balance is $" + myPiggyBank + ".");
		}
		else if(sumOfDealerCards <= 21 && sumOfDealerCards == secondHand && firstHand <= 21 && firstHand > secondHand)
		{
			//Condition reads: If dealer hand is less than or equal to 21 AND dealer hand is equal to
			//second hand AND second hand is less than first hand AND first hand is less than or equal to 21
			//--Player wins with first hand
			myCurrentBet *= multiplierFirstHand;
			myPiggyBank += myCurrentBet;
			System.out.println("\nYou beat me this round, nice going. Your balance is $" + myPiggyBank + ".");
		}
		else if((firstHand > 21 && secondHand > 21) || (sumOfDealerCards <= 21 
				&& ((firstHand > 21 && secondHand > 21) || (firstHand > 21 && secondHand < sumOfDealerCards)
						|| (secondHand > 21 && firstHand < sumOfDealerCards)
						|| (firstHand < sumOfDealerCards && secondHand < sumOfDealerCards))))
		{
			//Condition reads: If both player hands are over 21 OR (dealer's hand is less than or equal to 21 AND (both hands are over 21 
			//OR one of the hands is over 21 and the other one is less than the dealer's hand 
			//OR both hands are less than dealer's hand)) --Dealer wins both hands
			myCurrentBet *= (multiplierFirstHand + multiplierSecondHand);
			myPiggyBank -= myCurrentBet;
			System.out.println("\nI won this round, better luck next time. Your balance is $" + myPiggyBank+".");
		}
		else if(sumOfDealerCards <= 21 && sumOfDealerCards == firstHand && secondHand < firstHand)
		{
			//Condition reads: If dealer's hand is less than or equal to 21 AND dealer's hand is
			//equal to first hand AND second hand is less than first hand -- player loses second hand
			myCurrentBet *= (multiplierSecondHand);
			myPiggyBank -= myCurrentBet;
			System.out.println("\nI won this round, better luck next time. Your balance is $" + myPiggyBank+".");
		}
		else if(sumOfDealerCards <= 21 && sumOfDealerCards == secondHand && firstHand < secondHand)
		{
			//Condition reads: If dealer's hand is less than or equal to 21 AND dealer's hand is
			//equal to second hand AND first hand is less than second hand -- player loses first hand
			myCurrentBet *= (multiplierFirstHand);
			myPiggyBank -= myCurrentBet;
			System.out.println("\nI won this round, better luck next time. Your balance is $" + myPiggyBank+".");
		}
	}

	/**
	 *  This method performs the 'hit(s)' of a given sub-hand (after splitting)
	 *  <br>
	 *  pre: playerCurrentHand != null; decision != null</br>
	 *  post: none
	 *  @param sumOfCards player's old sum of cards; decision player's decision for current sub-hand
	 *  @return newSumOfCards player's new sumOfCards after having hit
	 */
	private int hitAfterSplit(Hand h, int sumOfCards, String decision) 
	{
		Card newPlayerCard = d.dealCard();
		h.addCard(newPlayerCard);
		sumOfCards = getOptimalSum(h);
		displayPlayersHand(h, sumOfCards);

		while(decision.equals("h") && sumOfCards < 21) 
		{
			System.out.print("\nEnter \"h\" for 'hit me' or \"s\" for 'stay': ");
			decision = sc.next();
			while(!checkInput(decision, false, false)) 
			{
				System.out.print("\nError in input. Enter \"h\" (for 'hit me'), \"d\" (for 'double'), or \"s\" (for 'stay'): ");
				decision = sc.next();
			}
			if(decision.equals("h")) 
			{
				newPlayerCard = d.dealCard();
				h.addCard(newPlayerCard);
				sumOfCards = getOptimalSum(h);
				displayPlayersHand(h, sumOfCards);
			}
		}

		return sumOfCards;
	}

	/**
	 *  This method is called when splitting did not take place. User may keep asking for cards (done recursively) 
	 *  until either the count goes over 21 or he/she decides to stop
	 *  <br>
	 *  pre: answer != null</br>
	 *  post: none
	 *  @param playerSums old sum of player's cards; sumOfDealerCards sum of dealer's cards; answer player's decision for current round
	 */
	private void playHand(int playerSum, int sumOfDealerCards, String answer) 
	{
		if(playerSum == 21 || answer.equals("s")) 
		{
			//Player doesn't need more cards (21 or stay)
			displayDealerHand();
			determineWinner(playerSum, sumOfDealerCards);
		}
		else if(!answer.equals("s"))
		{
			//deal a card for player
			playerHand.addCard(d.dealCard());
			playerSum = getOptimalSum(playerHand);

			displayPlayersHand(playerHand, playerSum);
			
			if(answer.equals("h") && playerSum < 21) 
			{
				//player hit and is still under 21
				System.out.print("\nEnter \"h\" (for 'hit me'), or \"s\" (for 'stay'): ");
				answer = sc.next();	 	
				while(!checkInput(answer, false, false)) 
				{
					System.out.print("\nError in input. Enter \"h\" (for 'hit me'), or \"s\" (for 'stay'): ");
					answer = sc.next();	 
				}
				playHand(playerSum, sumOfDealerCards, answer);
			}
			
			else 
			{
				//Player either hit and got 21 or more, or doubled. 
				//Either way, no need for more cards
				if(answer.equals("d")) 
				{
					//doubled
					myCurrentBet *= 2;
				}
				displayDealerHand();
				determineWinner(playerSum, sumOfDealerCards);
			}
		}
	}

	/**
	 *  This method determines who wins in a round where the user did not split
	 *  <br>
	 *  pre: none </br>
	 *  post: The user's piggy bank is adjusted according to the results
	 *  @param playerSum player's sum; dealerSum dealer's sum
	 */
	private void determineWinner(int playerSum, int dealerSum) 
	{
		if(playerSum <= 21 && (dealerSum < playerSum || dealerSum > 21))
		{
			myPiggyBank += myCurrentBet;
			System.out.println("\nYou beat me this round, nice going. Your balance is $"+myPiggyBank+".");
		}
		else if(playerSum == dealerSum && playerSum <= 21)
		{
			System.out.println("\nPush.");
		}
		else
		{
			myPiggyBank -= myCurrentBet;
			System.out.println("\nI won this round, better luck next time. Your balance is $"+myPiggyBank+".");
		}
	}

	/**
	 *  This method determines what the complete hand will be for the given round
	 *  <br>
	 *  pre: none</br>
	 *  post: Complete hand for the current round is layed out 
	 *  @return sum The hand's total sum.
	 */
	private int executeDealerHand() 
	{
		int sum = cardsAdder(dealerHand.myHand.get(0).worth, dealerHand.myHand.get(1).worth);
		while(sum < 17) 
		{
			dealerHand.addCard(d.dealCard());
			sum = getOptimalSum(dealerHand);
		}
		return sum;
	}

	/**
	 *  This method displays the player's current hand
	 *  <br>
	 *  pre: playerCurrentHand != null</br>
	 *  post: none
	 *  @param playerCurrentHand player's current hand; newSumOfCards player's current sum of cards
	 */
	private void displayPlayersHand(Hand playerCurrentHand, int newSumOfCards) 
	{
		System.out.println("\nYour hand:");
		for(int i=0; i<playerCurrentHand.size; i++) 
		{
			System.out.println(playerCurrentHand.myHand.get(i));
		}
		System.out.println("("+newSumOfCards+")");
	}

	/**
	 *  This method displays the dealer's current hand
	 *  <br>
	 *  pre: none </br>
	 *  post: none
	 *  @param sumOfDealerCards dealer's current sum of cards
	 */
	private int displayDealerHand() 
	{
		System.out.println("\nMy hand:");
		for(int i=0; i<dealerHand.size; i++) 
		{
			System.out.println(dealerHand.myHand.get(i));
		}

		System.out.println("("+getOptimalSum(dealerHand)+")");
		return getOptimalSum(dealerHand);
	}

	/**
	 *  This method displays both hands of the player after having split
	 *  <br>
	 *  pre: playerCurrentHand != null </br>
	 *  post: none
	 *  @param playerCurrentHand player's current hand
	 */
	private void displaySplitHand(Hand playerCurrentHand) 
	{
		System.out.println();
		for(int i=0; i<playerCurrentHand.size/2; i++) 
		{
			System.out.print(playerCurrentHand.myHand.get(i)+" ---> "+playerCurrentHand.myHand.get(i+2)+" ");
			System.out.println("("+cardsAdder(playerCurrentHand.myHand.get(i).worth, playerCurrentHand.myHand.get(i+2).worth)+")");
		}
	}

	/**
	 *  This method divides the player's hand into a sub-hand (when player splits)
	 *  <br>
	 *  pre: player != null </br>
	 *  post: Player's hand is divided into a sub-hand 
	 *  @param player the current hand of the player; setter a boolean value to determine whether we are splitting even or odd-numbered cards
	 *  @return h new sub-hand
	 */
	private Hand handSplitter(boolean setter) 
	{
		Hand h = new Hand();
		if(setter) 
		{
			for(int i=0; i<playerHand.size; i+=2) 
				h.addCard(playerHand.myHand.get(i));
		}
		else 
		{
			for(int i=1; i<playerHand.size; i+=2) 
				h.addCard(playerHand.myHand.get(i));
		}
		return h;
	}

	/**
	 *  This method calculates the Blackjack sum of two values
	 *  <br>
	 *  pre: none </br>
	 *  post: none
	 *  @param firstValue the first number that will be added; secondValue the second number that will be added
	 *  @return the sum of the two values
	 */
	private int cardsAdder(int firstValue, int secondValue) 
	{
		if(firstValue != 1 && secondValue != 1) 
		{
			return firstValue+secondValue;
		}
		else if(firstValue == 1 && secondValue == 1) 
		{
			return 12;
		}
		else if(firstValue == 1 && secondValue != 1 && ((11+secondValue)>=22)) 
		{
			return 1+secondValue;
		}
		else if(firstValue == 1 && secondValue != 1 && (11+secondValue<22)) 
		{
			return 11+secondValue;
		}
		else if(firstValue != 1 && secondValue == 1 && ((11+firstValue)>=22)) 
		{
			return 1+firstValue;
		}
		else if(firstValue != 1 && secondValue == 1 && (11+firstValue<22)) 
		{
			return firstValue+11;
		}
		return -1;
	}

	/**
	 *  This method will make adjustments if an ace could be used as 1 instead of 11 so the player or 
	 *  the dealer doesn't go over. Ex: A + 5 = 16, if you get a 10 after hitting this method will 
	 *  have the sum be 16 instead of 26
	 *  <br>
	 *  pre: currentHand != null </br>
	 *  post: none
	 *  @param currentHand player's or dealer's hand
	 *  @return call to overloaded compareAces method to get the actual return value
	 */
	private int getOptimalSum (Hand currentHand) 
	{
		int sumOfCardsWithoutAces = 0;
		int numberOfAces = 0;
		
		for(Card c: currentHand.myHand)
		{
			//Count number of aces in currentHand and determine total sum of cards that are not aces
			if(c.value == 1)
			{
				numberOfAces++;
			}
			else
			{
				sumOfCardsWithoutAces += c.worth;
			}
		}

		if(sumOfCardsWithoutAces > 21 || (sumOfCardsWithoutAces + numberOfAces > 21))
		{
			//Either already over or will go over even if using aces as ones, just add ones and return value
			return sumOfCardsWithoutAces += numberOfAces;
		}

		boolean done = false;
		int numberOfAcesUsedAsOnes = 0;
		int optimalSum = -1;

		while(!done)
		{
			optimalSum = sumOfCardsWithoutAces;

			//Add aces that can be used as 11
			for(int i = 0; i < numberOfAces - numberOfAcesUsedAsOnes; ++i)
			{
				optimalSum += 11;
			}

			//Add aces that have to be used as 1
			for(int i = 0; i < numberOfAcesUsedAsOnes; ++i)
			{
				optimalSum += 1;
			}

			if(optimalSum <= 21)
			{
				done = true;
			}
			else
			{
				numberOfAcesUsedAsOnes++;
			}
		}

		return optimalSum;
	}

	/**
	 *	This method validates the user input for a particular question
	 *  <br>
	 *  pre: answer != null </br>
	 *  post: none
	 *  @param answer user's current decision; yesOrNoQuestion boolean value for whether it's a yes or no question; mayDouble boolean value
	 *  for whether doubling is allowed in the current round
	 *  @return true if the input is valid, false otherwise
	 */
	private boolean checkInput (String answer, boolean yesOrNoQuestion, boolean mayDouble) 
	{
		if(yesOrNoQuestion) 
		{
			if(answer.equals("y") ||answer.equals("n"))
				return true;
		}
		else if(!mayDouble) 
		{
			if(answer.equals("h") || answer.equals("s"))
				return true;
		}
		else 
		{
			if(answer.equals("h") || answer.equals("s") || answer.equals("d"))
				return true;
		}
		return false;
	}
}
