/**
 * Deck simulator class
 * May 2013
 * This class allows to create a Deck of Card objects to play the game
 */

import java.util.*;

public class Deck 
{

	private ArrayList<Card> myDeck;
	private int top;
	private int deckSize;

	/**
	 *  Constructor. Creates a new deck, using an ArrayList of Cards as an internal container
	 *  <br>
	 *  pre: none</br>
	 *  post: a Deck object is created
	 */
	public Deck() 
	{
		myDeck = new ArrayList<Card>();
		top = 207;
		deckSize = 208;
		for(int i=1; i<=4; i++) 
		{
			for(int j=0; j<4; j++) 
			{
				for(int k=1; k<=13; k++) 
				{
					myDeck.add(new Card(k, j));
				}
			}
		}
		shuffle();
	}

	/**
	 *  Shuffle method takes two cards at random and switches their position for 50,000 times.
	 *  <br>
	 *  pre: none</br>
	 *  post: current Deck object is shuffled
	 */
	public void shuffle() 
	{
		Random generator = new Random();
		for(int i=1; i<=50000; i++) 
		{
			int firstRandom = generator.nextInt(208);
			int secondRandom = generator.nextInt(208);
			Card a = myDeck.get(firstRandom);
			Card b = myDeck.get(secondRandom);
			Card temp = a;
			myDeck.set(firstRandom, b);
			myDeck.set(secondRandom, temp);
		}
	}

	/**
	 *  Deals the top card from the deck (last card in the ArrayList)
	 *  <br>
	 *  pre: myDeck.size() > 0</br>
	 *  post: myDeck.size() has 1 Card object less
	 *  @return removen Card object
	 */
	public Card dealCard() 
	{
		Card returnee = myDeck.get(top);
		myDeck.set(top, null);
		top--;
		deckSize--;
		if(deckSize < 15) 
		{
			shuffle();
			top = 207;
			deckSize = 208;
		}
		return returnee;
	}

	/**
	 *  Accessor for deckSize
	 *  <br>
	 *  pre: none </br>
	 *  post: none
	 *  @return deckSize current size of the Deck object
	 */
	public int size() 
	{
		return deckSize;
	}

	/**
	 *  Overridden toString method
	 *  <br>
	 *  pre: none </br>
	 *  post: none
	 *  @return returnee String object with all Card's string representation
	 */
	public String toString() 
	{
		String returnee = "";
		for(int i=0; i<deckSize; i++) 
		{
			returnee += myDeck.get(i).toString() + "\n";
		}
		return returnee;
	}
}
