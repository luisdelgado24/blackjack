/**
 * Hand class
 * This class simulates a Hand of Card Objects
 * May 2013
 */

import java.util.ArrayList;
public class Hand 
{
	int size;
	ArrayList<Card> myHand;

	/**
	 *  Constructor
	 *  <br>
	 *  pre: none</br>
	 *  post: a new Hand object is created
	 */
	public Hand() 
	{
		myHand = new ArrayList<Card>();
		size = 0;
	}

	/**
     *  Accessor for the current hand's size
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return size current size of Hand object
	 */
	public int getSize() 
	{
		return size;
	}

	/**
     *  Method determines if the current hand is empty or not
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return boolean value for whether size is equal to 0
	 */
	public boolean isEmpty() 
	{
		return size == 0;
	}

	/**
	 *  Method removes the top card and returns it
	 *  <br>
	 *  pre: size > 0</br>
	 *  post: none
	 *  @return c removen Card object from Hand
	 */
	public Card getNext() 
	{
		size--;
		Card c = myHand.get(size);
		myHand.remove(size);
		return c;
	}

	/**
	 *  Method adds a card to the back of the hand
	 *  <br>
	 *  pre: none</br>
	 *  post: Hand object has one new Card
	 *  @param c Card object to be added
	 */
	public void addCard(Card c) 
	{
		myHand.add(c);
		size++;
	}

	/**
     *  Overridden toString method
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 */
	public String toString() 
	{
		String returnee = "";
		for(int i=0; i<size; i++) 
		{
			returnee += myHand.get(i).toString() + "\n";
		}
		return returnee;
	}

}
