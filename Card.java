/**
 * Card simulator class
 * May 2013
 * This class creates a Card object with which the game will be played
 */

public class Card 
{
	public int value;
	public int suit;
	public int worth;
	private static final int CLUBS = 0;
	private static final int DIAMONDS = 1;
	private static final int HEARTS = 2;
	private static final int SPADES = 3;

	/**
	 *  Constructor for specified card
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @param otherValue value that the new Card will carry; otherSuit suit that the new Card will carry
	 */
	public Card(int otherValue, int otherSuit) 
	{
		value = otherValue;
		suit = otherSuit;
		worth = assignWorth();
	}

	/**
	 *  Accessor method for card number
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return current Card's value
	 */
	public int getValue() 
	{
		return value;
	}

	/**
	 *  Accessor method for card suit
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return current Card's suit
	 */
	public int getSuit() 
	{
		return suit;
	}
	
	/**
	 *  Accessor method for card worth. The difference between worth and value is that
	 *  12's value is queen but its worth is 10.
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return current Card's worth
	 */
	public int getWorth() 
	{
		return worth;
	}

	/**
	 *  Return a String representing the card's suit.
	 *	(If the card's suit is invalid, "ERROR" is returned.)	
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return current Card's suit as a String
	 */
	public String getSuitAsString() 
	{	
		switch (suit) {
		case SPADES:   return "Spades";
		case HEARTS:   return "Hearts";
		case DIAMONDS: return "Diamonds";
		case CLUBS:    return "Clubs";
		default:       return "ERROR";
		}
	}

	/**
	 *  Return a String representing the card's value.
	 *  If the card's value is invalid, "ERROR" is returned.	
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return current Card's value as a String
	 */
	public String getValueAsString() 
	{
		switch (value) {
		case 1:   return "Ace";
		case 2:   return "2";
		case 3:   return "3";
		case 4:   return "4";
		case 5:   return "5";
		case 6:   return "6";
		case 7:   return "7";
		case 8:   return "8";
		case 9:   return "9";
		case 10:  return "10";
		case 11:  return "Jack";
		case 12:  return "Queen";
		case 13:  return "King";
		default:  return "ERROR";
		}
	}

	/**
	 *  Assigns a Blackjack 'worth' to all cards' values
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return current Card's worth
	 */
	public int assignWorth() 
	{
		switch(value) 
		{
		case 1:   return 1;
		case 2:   return 2;
		case 3:   return 3;
		case 4:   return 4;
		case 5:   return 5;
		case 6:   return 6;
		case 7:   return 7;
		case 8:   return 8;
		case 9:   return 9;
		case 10:  return 10;
		case 11:  return 10;
		case 12:  return 10;
		case 13:  return 10;
		default:  return -1;
		}
	}

	/**
	 *  Overridden toString method
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return current Card as a String
	 */
	public String toString() 
	{
		return getValueAsString() + " of " + getSuitAsString();
	}

	/**
	 *  Overridden equals method
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return true if Cards are equal, false otherwise
	 */
	public boolean equals(Card other) 
	{
		if(value == other.value && suit == other.suit) 
		{
			return true;
		}
		return false;
	}

	/**
	 *  compareValue method
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return if this Card's value is higher than other's value
	 */
	public boolean compareValue(Card other) 
	{
		if(value > other.value) 
		{
			return true;
		}
		return false;
	}

	/**
	 *  Comparator
	 *  <br>
	 *  pre: none</br>
	 *  post: none
	 *  @return true 1 if this Card's value is higher than c's value, 0 if they are equal, -1 otherwise
	 */
	public int compareTo(Object c) 
	{
		Card other = (Card) c;
		if(value > other.value) 
		{
			return 1;
		}
		if(value == other.value) 
		{
			return 0;
		}
		return -1;
	}

}